#include <stdio.h>
#include <stdlib.h>

int main(void) {
  int n,i=0;
  float *numeros;

  printf("Introduzca el numero de datos:\n");
  scanf("%d", &n);
  numeros = (float* ) malloc(n *sizeof(float));
  system("clear");

  while(i<n){
    printf("\ndato%d: ",i+1);
    scanf("%f",&numeros[i]);
    system("clear");
    i++;
  }

  float mayor = numeros[0],menor = numeros[0];
  float total=0;

  for(int j=0;j<n;j++){
    total += numeros[j];

    if(mayor<numeros[j])
      mayor = numeros[j];
    if(menor>numeros[j])
      menor = numeros[j];

    printf("|| %f ||\n",numeros[j]);
  }

  float media = total/n;

    printf("\nmayor: %f\nmenor: %f\nmedia: %f",mayor,menor,media);

  return 0;
}
