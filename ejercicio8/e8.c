#include <stdio.h>
#include <string.h>
#include <ctype.h>

char *pedirTexto();
void contarVocales(char *,int[]);
void imprimir(int[]);

char *pedirTexto(){

  char txt[500];
  char *pTxt = txt;
	printf("Introduzca un texto:\n");
  fgets(txt,500,stdin);

  return pTxt;
}

void contarVocales(char *texto, int *num){
  for(int i=0;i<5;i++)
    num[i]=0;

  while(*texto != '\0'){
    char letra = tolower(*texto); 
    switch(letra){
      case ('a'):
        num[0]++;
        break;
      case ('e'):
        num[1]++;
        break;
      case ('i'):
        num[2]++;
        break;
      case ('o'):
        num[3]++;
        break;
      case ('u'):
        num[4]++;
        break;
      default:
        break;
    }
    texto++;
  }
}

void imprimir(int *num){
  printf("|| a | %d ||\n", num[0]);
  printf("|| e | %d ||\n", num[1]);
  printf("|| i | %d ||\n", num[2]);
  printf("|| o | %d ||\n", num[3]);
  printf("|| u | %d ||\n", num[4]);
}

int main(void) {
  char *texto;
  int num[5];
  texto = pedirTexto();
  contarVocales(texto, num);
  imprimir(num);
  
  return 0;
}
