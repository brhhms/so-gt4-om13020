#include <stdlib.h>
#include <stdio.h>

int main(void){

  int n = 15;
  int *numeros;
  numeros = (int* ) malloc(n *sizeof(int));

  for (int i = 0; i < n; i++){
    numeros[i] = rand() % 101;
    printf("\n -->%d",numeros[i]);
  }

  int mayor;
  for(int x = 0 ; x < n ; x++){
    
    mayor = x;
    for (int y = x+1; y < n ;y++){
      if(numeros[mayor] < numeros[y]){
        mayor = y;
      }     
    }

    int temp = numeros[mayor];
    numeros[mayor] = numeros[x];
    numeros[x] = temp;
  }
 
 printf("\n\nnumeros ordenados:");
   for (int i = 0; i < n; i++){
    printf("\n -->%d",numeros[i]);
  }

return 0;
}
