#include <stdio.h>

int main(void) {
  float n1;
  float n2;
  float *p1;
  float *p2;
  n1 = 4.0;
  p1 = &n1;
  p2 = p1;
  n2 = *p2;
  n1 = *p1 + *p2;

  printf("\n n1 = %f",n1);
  printf("\n n2 = %f",n2);
  printf("\n &n1 = %p",&n1);
  printf("\n &n2 = %p",&n2);
  return 0;
}
