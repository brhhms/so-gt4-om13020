#include <stdio.h>
#include <stdlib.h>
#include<math.h>

int main(void) {

  double *coeficientes;
  int grado;

  printf("Introduzca el grado del polinomio:\n");
  scanf("%d", &grado);
  coeficientes = (double* ) malloc(grado *sizeof(double));
  system("clear");

  int i=grado;
  while(i >= 0){
    printf("\nx^%d: ",i);
    scanf("%lf",&coeficientes[i]);
    system("clear");
    i--;
  }
  
  double x;
  printf("\nx?:");
  scanf("%lf", &x);

  double resultado = 0;
  for(int j=grado;j>=0;j--){
    if(coeficientes[j] != 0){
      printf("%f",coeficientes[j]);
      if(j != 0)
        printf("X^%d + ",j);
    
    resultado += (coeficientes[j] * pow(x,j));
    }
  }
  printf("\nresultado = %f\n",resultado);

  return 0;
}
